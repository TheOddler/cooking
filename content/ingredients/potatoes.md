---
---

# Boiling

1. Put the potatoes fully under water, add some salt
2. To know when potatoes are ready, **poke them with a knife**
    * You should feel no hard parts inside
    * **Boiling time**: roughly **20 minutes** for *normal* sized potatoes.
    Bigger potatoes take longer, smaller potatoes (or potatoes cut in small pieces) take less time to boil.

# Baking

1. Cut in small pieces or slices
2. Add salt and pepper
3. Bake
    * On a low heat for a long time will give crispy potatoes
    * Short on high heat will give crunchy outside and soft inside
