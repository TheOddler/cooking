---
title: Stoofvlees met frieten
ingredients:
  apples: Appelen
  bay leaves: Laurier bladeren
  bread: Twee boterhammen
  chicory: Witloof
  mayonnaise: Mayonaise
  musterd: Mosterd
  onions: Twee grote ajuinen
  pepper: Peper
  potatoes: Friet aardappelen
  salt: Zout
  spice nut: Kruidnoot
  stoofvlees: 1-1.5kg Stoofvlees
requirements:
  - fryer
---
# Directions

## Stoofvlees

1. Neem een **grote pot** voor het stoofvlees
2. Snij twee **ajuinen** in stukjes en **bruin** deze in de pot
3. **Kruid** het stoofvlees stevig met **peper**, **zout** en **kruidnoot**
4. **Bak** het **stoofvlees**: op een hoog vuur schroei elk blokje stoofvlees dicht aan alle kanten
5. **Voeg vlees samen** met de **resten** in de pot bij de **ajuin**
6. Voeg **water** toe tot het stoofvlees onder staat
7. Voeg enkele **laurier** bladeren toe
8. Breng aan de **kook**
9. Vanaf dat het kookt zet het **vuur zeer laag**, plaats get **deksel** op de pot, en laat ongeveer **één uur stoven**
   * Laat het niet té gaar worden
10. Besmeer **twee boterhammen** met **veel mosterd**
11. Plaats de **boterhammen** met **mosterd naar beneden** op het **stoofvlees**, duw de boterhammen een klein beetje onder, dat ze helemaal doorweekt zijn
12. Laat **15 minuten** verder koken op een laag vuur
13. Roer alles onder elkaar
14. Laat rusten tot het eten, en warm het stoofvlees terug op net voor het eten
15. Serveer met veel mayonaise

## Frieten

1. **Snij frieten** van de aardappelen
2. Frituur de frieten één keer op **170 graden** om ze binnenkant de koken, laat je lichtjes donker geel worden, nog niet bruin
3. Wacht tot vlak voor het eten
4. Frituur de frieten een tweede keer nu op **190 graden** tot ze goudbruin zijn

## Witloof

1. Snij het **witloof** in kleine stukjes
2. Snij een **appel** of twee in kleine stukjes
3. Doe beide in een kom, voeg **mayonaise**, **zout** en **peper** toe
4. Mix goed door elkaar
