---
title: Tian Provençal
external_url: 'https://www.chefdehome.com/recipes/899/tian-orzo-vegetable-pasta-casserole'
image: >-
  https://media.chefdehome.com/750/0/0/ratatouille/baked-vegetables-orzo-pasta.jpg
ingredients:
  garlic: Some garlic
  lemon: 1 Lemon
  mozzarella: 1/2 Cup of grated mozzarella
  olive oil: Some olive oil
  onions: 1 Small red onion
  oregano: 1 Tablespoon of oregano
  parmesan: 1/2 Cup of grated parmesan cheese
  pasta: 1 Cup of Orzo Pasta (or something similar)
  squash: 1 Yellow Squash (thin slices)
  tomato sauce: 1 pot of tomato-basil sauce
  tomatoes: '3 Tomatoes (firm, thin slices)'
  zucchini: 1 Zucchini (thin slices)
optional_ingredients:
  chicken stock: 1 cup of chicken stock
tags:
  - pasta
---
# Directions

1. Pre-heat oven at **200 degrees celcius**.
2. Spray 9.5 inch baking dish with oil spray. Set aside.
3. In a **bowl**, add **orzo** (or somilar pasta), 1/4 cup **parmesan** cheese, **chilli flakes**, **oregano**, **basil**, 1/2 tablespoon **salt**, **lemon zest**, 1/2 tablespoon black **pepper**, and grated **garlic**. Mix well. Set aside.
4. Spread **tomato sauce** evenly in **baking dish** in single layer.
5. Next, gently spread **orzo mixture** evenly on top in single layer.
6. **Drizzle** a tablespoon of **olive oil** and a tablespoon of **lemon juice** on top. Set aside.
7. Top orzo layer with sliced **zucchini**, **squash**, **tomato** and **red onion.**
   * Arrange in alternately, starting with zucchini, squash, tomato and repeat to complete a spiral.
8. Add additional **moisture** (chicken stock, or just water). Enough for the pasta to absorb. Depending on how much liquid is already in the tomato sauce.
9. Season with generous pinch of **salt** and black **pepper**.
10. **Bake** until liquid in orzo has been absorbed. (about **20 minutes**)
11. Take casserole out and switch oven to broiler setting.
12. Sprinkle remaining **parmesan** and **mozzarella** cheese on top of baked veggies.
13. Return back to oven and **cook until cheese has melted and bubbling**.
14. Let cool for 5-10 minutes.
15. Garnish with fresh **chopped parsley**.
