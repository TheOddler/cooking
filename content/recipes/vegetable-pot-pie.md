---
title: Vegetable Pot Pie
external_url: https://www.tasteofhome.com/recipes/quick-and-easy-vegetable-pot-pie/
image: https://www.tasteofhome.com/wp-content/uploads/0001/01/Fast-French-Veggie-Pot-Pie_EXPS_TOHFM20_241042_B09_19_7b.-696x696.jpg
ingredients:
  butter: 2 tablespoons butter
  pepper: 1/3 tablespoon pepper
  nutmeg: 1/3 tablespoon nutmeg
  flour: 2 tablespoons all-purpose flour
  parmesan: 1/4 cup grated Parmesan cheese
  ginger: 1/3 tablespoon ginger
  mustard: 1 tablespoon (Dijon) mustard
  olive oil: 1 tablespoon olive oil
  lentils: 1 can (15 ounces) lentils, drained
  vegetables: 3 cups frozen mixed vegetables, thawed
  pie crust: 2 sheet refrigerated pie crust
  vegetable broth: 1 cup vegetable broth
  salt: 1/2 teaspoon salt
  cloves: 1/6 tablespoon ground cloves
optional_ingredients:
  chicken broth: Optionally use chicken broth instead of vegetable broth
tags:
  - pie
  - veggie
requirements:
  - oven
---
# Directions

1. Preheat oven to 375°.
2. In a large skillet, melt butter over medium heat.
3. Add vegetables and lentils; cook and stir until heated through, 3-5 minutes.
4. Stir in flour until blended;
5. gradually whisk in broth.
6. Bring to a boil, stirring constantly; cook and stir until thickened, 1-2 minutes.
7. Stir in mustard, spices and salt.
8. Transfer to a pie plate.
9. Place pie crust over filling. 
10. sprinkle with Parmesan.
11. Bake until golden brown, 30-35 minutes.
12. Cool 5 minutes before serving.
