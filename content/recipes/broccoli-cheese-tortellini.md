---
title: Broccoli Cheese Tortellini
external_url: https://www.tasteofhome.com/recipes/broccoli-cheese-tortellini/
image: https://www.tasteofhome.com/wp-content/uploads/2017/10/exps29098_SD1440070D31B.jpg
ingredients:
  broccoli: One broccoli
  cream: 25cl of cream
  parmesan: 250g Parmesan or mixed cheese
  pepper: Pepper
  tortellini: A pack of cheese tortellini (250-300g)
optional_ingredients:
  cheese: Mixed cheese as an alternative to or in combination with parmesan
tags:
  - veggie
  - pasta
---
# Directions

1. In a large saucepan, cook cream and broccoli, uncovered, over medium-low heat for 5-6 minutes or until broccoli is crisp-tender.
2. Meanwhile, cook tortellini according to package directions.
3. Stir 2 cups cheese and pepper into broccoli mixture.
4. Bring to a boil. Reduce heat; simmer, uncovered, for 8-10 minutes or until mixture is thickened, stirring occasionally.
5. Drain tortellini; add to sauce and toss to coat.
6. Sprinkle with a bit of extra cheese.
