---
title: Pancakes
external_url: 'https://www.libelle-lekker.be/bekijk-recept/3745/pannenkoeken-2'
ingredients:
  eggs: 3 eggs
  flour: 250 g of flour
  milk: 5 dl of milk
---
# Directions

1. **Mix everything** together until there are no more clumps
2. Move a spoonful to a pan and **bake**
