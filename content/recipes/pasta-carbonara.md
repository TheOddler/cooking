---
title: Pasta Carbonara
external_url: "https://www.simplyrecipes.com/recipes/spaghetti_alla_carbonara/"
image: https://www.simplyrecipes.com/wp-content/uploads/2012/02/pasta-carbonara-vertical-a-1200-600x892.jpg
ingredients:
    olive oil: 1 tablespoon extra virgin olive oil
    bacon: 1/2 pound pancetta or thick cut bacon, diced
    eggs: 3-4 whole eggs
    parmesan: 1 cup grated parmesan cheese
    spaghetti: 1 pound spaghetti pasta (or bucatini or fettuccine)
    salt: Salt
    pepper: black pepper
optional_ingredients:
    garlic: 1-2 garlic cloves, minced, about 1 teaspoon
tags:
    - pasta
---

# Directions

1. **Cook pasta** in plenty of salted water in a large pot
2. **Beat eggs** and **half** of the **cheese** in a small bowl
3. **Drain pasta** and let rest
    * Optionally keep some of the water for later
4. **Reuse** the large **pot** to sauté the **bacon** and optionally **garlic**
5. Toss drained **pasta** back into the **pot** with the bacon
    * Optionally let the pasta gain some heat again if it cooled off too much
6. Mix the beaten **egg mixture** with the **pasta**
    * The pasta should cook the egg mixture
7. Add **salt** to taste
8. Optionally add in the water you kept earlier to keep the pasta from drying out
9. Serve with more cheese on top
