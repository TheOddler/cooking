---
title: Shepherd's Pie
external_url: "https://www.reddit.com/r/food/comments/dlsc4k/homemade_shepards_pie_with_lamb/f4ujfcg/"
image: https://www.seriouseats.com/images/2016/10/20161012-shepherds-pie-vicky-wasik-Version-A-22.jpg
ingredients:
    ground beef: 800g ground beef (and/or lamb)
    onions: 1 large onion, diced
    celery: 1 celery stalk, diced
    carrots: 1 large carrot, diced
    peas: 1 cup frozen peas
    garlic: 1 garlic clove, crushed/pureed
    beef stock: 500ml/most of a pint beef stock
    flour: All purpose flour
    tomato puree: 1 tablespoon tomato puree
    Worcestershire sauce: 1 tsp Worcestershire sauce
    bay leaves: 1 bay leaf
    thyme: Fresh thyme
    potatoes: 1kg potatoes
    butter: Butter
    milk: Milk
optional_ingredients:
    red wine: 1 glass red wine
    cheese: Cheese of choice or butter
    maize: 1 cup of maize
requirements:
    - oven
tags: 
    - pie
---

# Directions

1. In a large **sauté** pan fry the **onions**, **carrots**, and **celery** in your oil of choice until softened and beginning to colour
2. Add the **garlic** and **peas** and fry for a couple of minutes more
    * Optionally add **maize** here too
3. **Remove the veg** from the pan, increase the heat, and **brown the meat** well, in batches if necessary, seasoning as desired
4. **Return the veg** to the pan, add the **tomato paste**, stir, and cook out for a minute
5. Add enough **flour** to absorb any fat and liquid in the pan, about a heaped tablespoon, and cook out for a couple of minutes over low heat
6. If using, add the **wine** and stir well to pick up all the tasty bits from the pan
7. Add the stock, herbs and Worcestershire sauce and stir well
8. Bring to a simmer and **cook for 30 minutes** or so, stirring occasionally, **until the liquid is reduced** and thickened and you have a kind of thick meat sauce
9. **Transfer** to an oven-proof casserole dish and **let cool completely**
10. **Remove the herbs**
11. At this stage you can cover and put it in the fridge until you're ready to finish it off
    * If you don't cool the meat the mashed potato tends to sink into it and you get a weird soggy Shepherd's Pie
12. When you're ready, preheat the oven to **200°C**
13. Make **mashed potatoes**
14. Use a large spoon (or a piping bag if you're being really fancy) to **transfer the mash to the meat dish**
    * Use a fork to add some ridges to the potatoes - these catch and crisp in the oven, adding texture
15. Top with grated **cheese** or some dots of butter
16. Place in the hot **oven** for 20 minutes or until the top has turned golden brown and crisp
17. Serve with the veg of your choice - many people also make extra gravy to pour over
