---
title: Ratatouille
external_url: https://www.chefdehome.com/recipes/561/ratatouille
image: https://media.chefdehome.com/750/0/0/ratatouille/ratatouille-recipe-pixar-movie.jpg
ingredients:
  butter: Butter
  chili: 1/2 tsp Chili Flakes
  pepper: Black Pepper
  nutmeg: 1/8 tsp Nutmeg
  flour: All-Purpose Flour
  eggplant: 1 Eggplant (thin eggplant, thin sliced)
  olive oil: 2 tablespoon Olive Oil
  garlic: 2 Garlic (cloves, minced)
  tomato sauce: 1-2 Cup Tomato Sauce (good quality tomato basil sauce)
  potatoes: 2-3 Potatoes (medium size, thin sliced)
  salt: Salt
  cheese: 1 Cup of mixed grated cheese
  zucchini: 1 Zucchini (thin sliced)
  milk: 1 Cup Milk (2% or whole milk)
  thyme: 3-4 Thyme (sprigs)
  squash: 1 Yellow Squash (thin sliced)
  bell pepper: 1 Red Bell Pepper (thin sliced)
tags:
  - veggie
requirements:
  - oven
---

# Directions

1. Cut **potatoes**, **eggplant**, **squash**, **zucchini** and **bell pepper** in **thin equal slices**
    * Optionally slightly boil potatoes
2. Make little veggie stacks
    * Alternating **potatoes**, **eggplant**, **squash**, **zucchini** and **bell pepper** slices
3. Make **béchamel sauce**:
    * Melt some butter in sauce pan
    * Add flour until all butter is soaked up and cook until it smells like cookies
    * Add milk 
    * Add cheese and cook until sauce thickens and coats the back of a spoon
    * Season with salt, lots of nutmeg, and black pepper
4. Spread **tomato sauce** in a **baking dish**
5. Mix in **minced garlic**, **thyme**, **red chili flakes**, **salt** and **olive oil**
6. Drizzle **béchamel sauce** on the top, swirl into the tomato sauce
7. Put **veggie stacks** sideways in rows into the **baking dish**
    * Optionally make a spiral pattern in a round baking dish, or any pattern you like
8. Bake at **180°** for about an **hour**, the tomato sauce should be bubbling on the side
    * Optionally cover with parchment paper
