---
title: Chicken Pot Pie
external_url: https://www.tasteofhome.com/recipes/favorite-chicken-potpie/
image: images/chicken_pot_pie.jpg
ingredients:
  butter: 1 cup butter, cubed
  pepper: Pepper to taste
  flour: 1/2 cup all-purpose flour
  peas: 1 cup frozen peas
  chicken broth: 1 or 2 cups chicken broth
  corn: 1 cup frozen corn
  onions: A chopped onion
  carrots: 1 or 2 sliced carrots
  pie crust: 2 sheets refrigerated pie crust
  chicken: 2 cubed and cooked chicken breasts
  potatoes: 3 diced peeled potatoes
  salt: Salt to taste
  milk: 1/2 cup whole milk
  thyme: Dried thyme to taste
tags:
  - pie
requirements:
  - oven
---
# Directions

1. Boil **potatoes** and **carrots** until crisp-tender; drain
2. **Create filling** in a large skillet

   * Heat **butter** over medium-high heat
   * Add **onion**; cook and stir until tender
   * Stir in **flour** and **seasonings** until blended
   * Gradually stir in **broth** and **milk**
   * Bring to a **boil**, stirring constantly, **until thickened**
   * Stir in **chicken**, **peas**, **corn** and **potato mixture**
   * Remove from heat
3. Put **pie crust** into a pie plates; trim even with rims
4. Add **chicken mixture**
5. Place **second pie crust** over filling. Trim, seal and flute edges. Cut slits in tops
6. **Bake about an hour** until crust is lightly browned at **160°**

# Freeze option

Cover and freeze unbaked pies.
To use, remove from freezer 30 minutes before baking (do not thaw).
Preheat oven to 425°.
Place pies on baking sheets; cover edges loosely with foil.
Bake 30 minutes.
Reduce oven setting to 350°; bake 70-80 minutes longer or until crust is golden brown and a thermometer inserted in center reads 165°.