---
title: Chicken Mushroom Pie
external_url: 'https://www.bbc.co.uk/food/recipes/chickenandmushroompi_89034'
ingredients:
  butter: 50g Butter
  chicken: 3 Chicken breasts
  chicken stock: 200ml Chicken stock
  flour: 2 Tablespoons of plain flour
  garlic: 1 Garlic clove
  milk: 300ml Milk
  mushrooms: 150g Mushroom
  nutmeg: Nutmeg to taste
  olive oil: 2 Tablespoons of olive oil
  onions: 1 Onion
  parsley: A small handful of parsley
  pepper: Pepper to taste
  pie crust: 2 Sheets refrigerated pie crust
  salt: Salt
tags:
  - pie
requirements:
  - oven
---
# Directions

1. Preheat the oven to **200C**.
2. Heat the oil in a frying pan, add the **chicken** and **fry** until the chicken begins to **turn white**.
3. Add the **mushrooms** and continue to fry until the chicken is **golden-brown**.
4. **Remove** the chicken and mushrooms from the pan and **set aside**.
5. Add the **onion** and garlic to the **same pan** and **fry** for 2-3 minutes or until softened.
6. Remove from the heat and **set aside** with the chicken and mushrooms.
7. **Melt** the **butter** in a **saucepan**, stir in the **flour** and **cook** for about **three minutes**, **stirring constantly** until it has formed a thick **smooth paste** (this is called a roux).
8. Mix the **milk** and **stock** together in a **jug**, then add the **nutmeg**, white **pepper** and **salt**, to taste.
9. **Pour** the liquid slowly **into** the **flour mixture**, **whisking** all the time until **smooth**.
10. **Simmer** over a **gentle heat**, stirring constantly, for about five minutes or **until** the sauce has **thickened**.
11. **Stir** in the chopped **parsley**.
12. **Pour** the sauce **over** the **chicken and mushroom mixture**. Mix well.
13. Put one **pie crust** in a pie plate.
14. Spoon **mixture into** the **pie crust**.
15. Put second **pie crust** on top.
16. Make some **slits** in the top of the pie to allow steam to escape.
17. **Bake** in the oven for 20-25 minutes or until golden-brown on top.
