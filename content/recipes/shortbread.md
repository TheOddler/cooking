---
title: Shortbread
external_url: 'https://www.bbc.co.uk/food/recipes/shortbread_1290'
image: >-
  https://ichef.bbci.co.uk/food/ic/food_16x9_1600/recipes/shortbread_1290_16x9.jpg
ingredients:
  butter: 125g Butter
  flour: 180g plain flour
  sugar: 55g Sugar (caster or fine)
optional_ingredients:
  sugar: Extra sugar on top
requirements:
  - oven
---

# Directions

1. **Beat** the **butter** and the **sugar** together until **smooth**.
2. Stir in the **flour** to get a **smooth** paste.
3. Turn on to a work surface and gently **roll out** until the paste is **1cm** thick.
4. **Cut** into rounds or fingers and place onto a baking tray.
   * Optionally sprinkle with caster sugar
5. **Chill** in the **fridge** for **20 minutes**.
6. **Bake** in the oven until pale golden-brown, put them in while heating up the oven.
