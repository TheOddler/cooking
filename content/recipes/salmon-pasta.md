---
title: Salmon Pasta
image: https://www.cherryonmysundae.com/wp-content/uploads/2012/05/smoked-salmon-pasta-5.jpg
ingredients:
    pasta: Any pasta you like
    salmon: 200g smoked salmon
    leeks: 3 Leeks
    champignons: 250g of champignons
    cream: 25cl Cream
    salt: Salt to taste
    pepper: Pepper to taste
    olive oil: Olive oil
optional_ingredients:
    peas: Half a cup of peas
    asparagus: Half a cup of asparagus
    tomatoes: 2 Tomatoes
    cheese: Parmesan or mixed grated cheese
tags:
    - pasta
    - fish
---

# Directions

1. Cook the **pasta** in plenty of water
2. **Clean the leeks** thoroughly and cut in small pieces
3. Start the rest of the recipe when putting the pasta in water, the remaining steps won't take long
4. In a large pot add some olive oil and **stew** the **leeks** until tender
5. Cut the **champignons** in pieces and add to the leeks
    * Optionally add **peas** or **asparagus** here too
6. Add the **cream**, **salt** and **pepper**
7. Let **cook** a few minutes **until tender**
8. While cooking **cut** the smoked **salmon** in pieces
9. Add pasta, champignon and leek mix and the salmon pieces together in one pot and **mix everything**
    * Optionally add the **tomatoes** here (cut in small pieces)
