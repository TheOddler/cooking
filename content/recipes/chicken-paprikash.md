---
title: Chicken Paprikash
external_url: 'https://www.chefdehome.com/recipes/895/chicken-paprikash'
image: 'https://media.chefdehome.com/740/0/0/hungarian/chicken-paprikash-pasta.jpg'
ingredients:
  butter: Unsalted Butter
  chicken: 500g diced chicken
  chicken stock: Chicken stock
  flour: 1.5 Tablespoon of all-purpose flour
  garlic: 3 large cloves of garlic
  olive oil: 1 Tablespoon of Olive Oil
  onions: 1 Cup of onions
  paprika: 2 Tablespoons of paprika powder
  parsley: Some parsley
  pasta: Curly pasta
  pepper: 1 tablespoon of cayenne pepper
  salt: Salt and pepper
  sour cream: 3/4 Cup of sour cream
  tomato puree: 2 Tablespoons of tomato puree or paste
tags:
  - pasta
---
# Directions

1. Heat **olive oil** in a **sauté pan**.
2. **Season** diced **chicken** with generous pinch of **salt**, black **pepper**, and a tablespoon of **paprika powder**.
3. Add seasoned **chicken** in **pan** and **cook** stirring often until chicken is no longer pink.
4. **Remove chicken** with a slotted spoon into a plate. **Set aside**.
5. Return pan to heat.
6. Add **butter**. Let it **melt**.
7. Add **onion** and **garlic**.
8. **Sauté until** **onions** are **soft**. (3-4 minutes) Stir often. Don't let garlic burn. 
9. Sprinkle **flour** on top of cooked onions **evenly**. Let the flour soak up some of the moisture.
10. Continue **cooking** for **1-2 minute**. Don't let flour burn.
11. Now add more **paprika powder**, **cayenne** and **tomato paste** (or tomato puree, if using).
12. **Cook** another **1 minute**.
13. Add **chicken stock**, **1/2 cup water**, and 1/2 tablespoon of **salt**. Mix well. Make sure there are no lumps.
14. Return **chicken** to pan.
15. Bring to **boil**, then reduce **heat to medium low** and cook for **10 minutes** or until sauce is thick and chicken is cooked through.
16. Remove pan from heat, mix in **sour cream*** to taste. Taste and adjust salt. Mix in chopped **parsley**.
17. Cook **pasta** and serve the sauce over it.

\* To avoid lumps: Mix sour cream with 2 tbsp of water and use hand whisk to whisk fully before adding to sauce.

\* To avoid curdling temper the sour cream: Add 1/4 cup sauce into sour cream and mix well, so the sour cream gets less of the heat at once. Then add that mixture to the rest of the sauce.
