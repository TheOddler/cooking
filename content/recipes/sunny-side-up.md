---
title: Sunny Side Up
ingredients:
  egg: A single egg
  salt: Some salt
---
# Directions

1. Put the **pan** on the stove and add some **oil**
2. Add the **eggs** to the pan
3. Fry the eggs on a **low fire** until the egg white is no longer runny
