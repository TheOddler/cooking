let selectedIngredients = new Set()

function toggleSelected(el) {
    let isNowSelected = el.classList.toggle("active")
    let ingredientName = el.dataset.ingredient
    if (isNowSelected) {
        selectedIngredients.add(ingredientName)
    }
    else {
        selectedIngredients.delete(ingredientName)
    }
    updateHidden()
}

function updateHidden() {
    var pages = document.getElementsByClassName("page")
    //console.log(pages)
    for (let page of pages) {
        let pageIngredients = page.dataset.ingredients.split(";")
        let shouldBeVisible = true
        for (let ingredient of selectedIngredients) {
            if (!pageIngredients.includes(ingredient)) {
                shouldBeVisible = false
            }
        }
        page.classList.toggle("hidden", !shouldBeVisible)
    }
}