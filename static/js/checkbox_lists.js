$(".content li").prepend('<input type="checkbox"> ')

$(".content li").each(function () {
    let childListIndex = $(this).children("ol, ul").index()
    if (childListIndex == -1) {
        $(this).contents().wrapAll("<label></label>")
    }
    else {
        $(this).contents().slice(0, -2).wrapAll("<label></label>")
    }
})
