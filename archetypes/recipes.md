---
title: Recipe Name
external_url: "www.optional.com"
image: "optional img url"
ingredients:
  butter: Lots of butter
optional_ingredients:
  red wine: A glass of red wine if you're fancy
tags:
  - pie
requirements:
  - fryer
---

# Directions

1. Step 1
2. Step 2
