# My Recipes


# Setup

1. First you'll need to clone the repo somewhere:
	* in your terminal: `git clone https://gitlab.com/TheOddler/cooking.git`
	* if you don't have Git get it here: https://git-scm.com/
2. Now you should have a folder named `cooking` with all the project files in it
3. Install node (which include `npm`): https://nodejs.org/
4. Run `npm ci` in the cooking folder to install all dependencies
	* Alternatively run `npm install` if you want to update the dependencies or want to add new functionality beyond just new recipes

# Start debug server

`npm run serve`

# Thanks

Custom widget based on: https://www.netlifycms.org/docs/custom-widgets/
